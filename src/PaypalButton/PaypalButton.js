import React from 'react';
import { PayPalButton } from "react-paypal-button-v2";

const paypal = require('paypal-rest-sdk');

class PaypalButton extends React.Component {
  
  render() {

    //const { amount, onSuccess, onCancel, paypalClick, currency } = this.props;
    const { amount, currency, onApprove } = this.props;

    const  card = "card";
    const credit = "Credit";

    return (
        <div >
            <PayPalButton
                amount={amount}
                currency={currency}
                onApprove={(data, actions) => onApprove(data, actions)}
                options={{
                  disableFunding: credit,
                  //clientId: "AZUN7HUzjhtkht9ce5eEdEsEOFEjlhfNjzduee3pwezvWyqUkjOOl8_ppv7hS3eOhngLxmlwqzL1ltrF" // Live Client ID
                  clientId: "AayFaXuRpa1zfNlKVhg7VBU9WwvQQlLM3ZColQtwiEYuSp_Ff6c3J-zmOLCmyHP7HdlKhTFkcJx7249I" //Sandbox Client ID
                }}
            />
        </div>
    );

  }
}
export default PaypalButton;

//createOrder={(data, actions) => createOrder(data, actions)}
//                onClick={paypalClick}
//                onCancel={(data) => onCancel(data)}