import React from 'react';
import './InfoBox.css';

class InfoBox extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            
        };

    }
    
    //Lifecycle method for after Header component has mounted to the DOM
    componentDidMount(){ 
    
    }

    //Lifecycle event preparing Header component to unmount from DOM
    componentWillUnmount(){
        
    }

    hideModal(event){
        document.getElementById((this.state.id === undefined ? "modalContainer" : this.state.id)).style.display = "none";
    }

    //Render the Header component to the DOM/Screen
    render(){

        var addText1 = "There are over 350,000 baptized members and over 1200 churches in the Wisconsin Evangelical Lutheran Synod (WELS). Sometimes, I forget how many brothers and sisters I have in Christ all over the country and around the world. I built this platform in hopes that we could help support our churches spread across the globe one dollar at a time!";
        var addText2 = "I hope no one ever feels an obligation to give a dollar every week. If you want to do so, that is great, and I thank you. :) But this is a free will donation, and will remain as such. Any amount will go a long way. Even just a prayer.";
        var addText3 = "I hope this platform can serve its purpose well and bring all of us closer together as a family in our Lord and Savior Jesus Christ. Please do not ever be afraid to get ahold of me if you have any questions or are experiencing problems with this page. (wymtowne@gmail.com)";
        var addText4 = "Thank you very much, and God bless. ~ Wyatt Towne";

        return(
            <div className="infoBoxShader">
                <div className="infoBoxWrapper" onClick={this.props.tileClickEvent}>
                    <div className="infoBoxContent">
                        <div className="infoBoxTextWrapper">
                            <p className="infoBoxText">{addText1}</p>
                            <br/>
                            <p className="infoBoxText">{addText2}</p>
                            <br/>
                            <p className="infoBoxText">{addText3}</p>
                            <br/>
                            <p className="infoBoxText">{addText4}</p>
                        </div>
                        <iframe className="popoutVideo" src="https://www.youtube.com/embed/RV6cvKOH4jo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div className="InfoBox-Button-Wrapper">
                            <button className="infoBoxConfirm" onClick={this.props.confirm}>{"Close"}</button>
                        </div>
                    </div>
                </div>
            </div>
        );
            
    }s
}

export default InfoBox;

//<Hamburger />
//
//"react-router-dom": "^6.0.0-alpha.1",