import React from 'react';
import './ConfirmDonation.css';

class ConfirmPurchase extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            
        };

    }
    
    //Lifecycle method for after Header component has mounted to the DOM
    componentDidMount(){ 
    
    }

    //Lifecycle event preparing Header component to unmount from DOM
    componentWillUnmount(){
        
    }

    hideModal(event){
        document.getElementById((this.state.id === undefined ? "modalContainer" : this.state.id)).style.display = "none";
    }

    //Render the Header component to the DOM/Screen
    render(){

        var passage = "You will be enriched in every way so that you can be generous on every occasion, and through us your generosity will result in thanksgiving to God. ~ 2 Corinthians 9:11";
        var btnText = "Close";

        return(
            <div className="confirmDonation" id="confirmDonationContainer" onClick={this.props.tileClickEvent}>
                <div className="confirmDonationContent">
                    <p className="confirmDonationText" id="confirmDonationText">{passage}</p>
                    <div className="ConfirmDonation-Button-Wrapper">
                        <button className="confirmDonationConfirmText" onClick={this.props.confirm}>{btnText}</button>
                    </div>
                </div>
            </div>
        );
            
    }s
}

export default ConfirmPurchase;

//<Hamburger />
//
//"react-router-dom": "^6.0.0-alpha.1",